//! \file    JElib.c
//! \brief   Source JElib file

#ifndef JELIB_C // because JElib.h includes JElib.c
#define JELIB_C
#include "JElib.h"

static _jeState_t _jeState = { false };

#define _JE_IMPORT(lib, retval, name, params) \
	retval (WINAPI* _je ##name) params = NULL
#include "JEImportList.h"
#undef _JE_IMPORT

#if defined (_MSC_VER) && (_MSC_VER >= 1400) // MSVC 8 (2005) or greater
	#pragma warning (disable: 4996)
	// warning C4996: '...': This function or variable may be unsafe.
	// Consider using strcpy_s instead. To disable deprecation, use
	// _CRT_SECURE_NO_WARNINGS. See online help for details.
#endif

void jeCreateWindowEx(int width, int height, const char* name)
{
	JE_ASSERT(width > 0 && height > 0);
	JE_ASSERT(name);

	if (_jeState.initialized)
		jeError("Нельзя создать более, чем одно окно.");

#define _JE_IMPORT(lib, retval, name, params) \
     _je ##name = (retval (WINAPI*) params) _jeDllImport (lib ".dll", #name)
#include "JEImportList.h"
#undef _JE_IMPORT

	_jeState.initialized = true;
	_jeState.windowSizeX = width;
	_jeState.windowSizeY = height;

	InitializeCriticalSection(&_jeState.dcCriticalSection);
	InitializeCriticalSection(&_jeState.inputCriticalSection);
	
	WNDCLASSEX wc;
	memset(&wc, 0, sizeof (WNDCLASSEX));
	wc.cbSize        = sizeof (wc);
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = _jeWndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = GetModuleHandle(NULL);
	wc.hIcon		 = LoadIcon (GetModuleHandle (NULL), IDI_APPLICATION);
	wc.hCursor       = LoadCursor (NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) _jeGetStockObject (WHITE_BRUSH);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = "jeWindowClass";

	JE_ASSERT (RegisterClassEx(&wc));

	int style = WS_POPUP | WS_BORDER | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
	int sizeX = width  + 2 * GetSystemMetrics(SM_CXFIXEDFRAME);
	int sizeY = height + 2 * GetSystemMetrics(SM_CXFIXEDFRAME) + GetSystemMetrics(SM_CYCAPTION);

	_jeState.window = CreateWindow("jeWindowClass", name, style,
								   (GetSystemMetrics (SM_CXSCREEN) - width) / 2,
								   (GetSystemMetrics (SM_CYSCREEN) - height) / 2,
								   sizeX, sizeY, NULL, (HMENU) NULL, GetModuleHandle (NULL), NULL);

	JE_ASSERT(_jeState.window != NULL);
	ShowWindow(_jeState.window, SW_SHOW);

	HDC wndDC = GetDC(_jeState.window);
	_jeState.windowDC = _jeCreateCompatibleDC(wndDC);
	JE_ASSERT(_jeState.windowDC);
	HBITMAP bitmap = _jeCreateCompatibleBitmap(wndDC, width, height);
	_jeSelectObject(_jeState.windowDC, bitmap);
	ReleaseDC(_jeState.window, wndDC);
	
	_jeSetBkMode(_jeState.windowDC, TRANSPARENT);

	_jeState.readyWindowDC = jeCreateDC(width, height);
	
	jeSetMainDCWorking();
	jeHideConsole();

	int i;
	for (i = 0; i < JE_NUM_KEY_CODES; i++)
		_jeState.keys[i] = false;
}

void jeCreateWindow(int width, int height)
{
	char name[MAX_PATH];
	_jeGetModuleFileName(name);
	jeCreateWindowEx(width, height, name);
}

void jeUpdate()
{
	JE_FUNCTION_START(jeUpdate);	
	
	EnterCriticalSection(&_jeState.dcCriticalSection);
	_jeBitBlt(_jeState.readyWindowDC, 0, 0, _jeState.windowSizeX, _jeState.windowSizeY, 
		_jeState.windowDC, 0, 0, SRCCOPY);
	LeaveCriticalSection(&_jeState.dcCriticalSection);

	UpdateWindow(_jeState.window);
	InvalidateRect(_jeState.window, NULL, false);

	MSG message;
	PeekMessage(&message, NULL, 0, 0, PM_REMOVE);

	TranslateMessage(&message);
	DispatchMessage(&message);
}

void jeWait()
{
	JE_FUNCTION_START(jeWait);

	while (true) // because after window close calling exit(0);
	{
		jeUpdate();
		jeSleep(10);
	}
}

void jeSetColor(COLORREF color)
{
	JE_FUNCTION_START(jeSetColor);
	jeSetColorEx(color, _jeState.lineWidth);
}

void jeSetColorEx(COLORREF color, int size)
{
	JE_FUNCTION_START(jeSetColorEx);

	_jeState.color = color;
	_jeState.lineWidth = size;

	HPEN pen = NULL;

	if (color != JE_TRANSPARENT)
		pen = _jeCreatePen(PS_SOLID, size, color);
	else
		pen = _jeCreatePen(PS_NULL, 1, JE_BLACK);

	JE_ASSERT(pen != NULL);

	_jeDeleteObject((HGDIOBJ) _jeSelectObject(_jeState.workingDC, (HGDIOBJ) pen));
	_jeSetTextColor(_jeState.workingDC, color);
}

void jeSetFillColor(COLORREF color)
{
	JE_FUNCTION_START(jeSetFillColor);

	_jeState.fillColor = color;

	if (color != JE_TRANSPARENT)
	{
		HBRUSH brush = _jeCreateSolidBrush(color);
		JE_ASSERT(brush != NULL);
		_jeDeleteObject((HGDIOBJ) _jeSelectObject(_jeState.workingDC, (HGDIOBJ) brush));
	}
	else
	{
		HGDIOBJ brush = _jeGetStockObject(HOLLOW_BRUSH);
		JE_ASSERT(brush != NULL);
		_jeDeleteObject((HGDIOBJ) _jeSelectObject(_jeState.workingDC, brush));
	}
}

void jeSetLineWidth(int width)
{
	JE_FUNCTION_START(jeSetLineWidth);
	jeSetColorEx(_jeState.color, width);
}

COLORREF jeGetColor()
{
	JE_FUNCTION_START(jeGetColor);
	return _jeState.color;
}

COLORREF jeGetFillColor()
{
	return _jeState.fillColor;
}

void jeDrawLine(int x1, int y1, int x2, int y2)
{
	_jeMoveToEx(_jeState.workingDC, x1, y1, NULL);
	_jeLineTo(_jeState.workingDC, x2, y2);
}

void jeDrawEllipse(int x1, int y1, int x2, int y2)
{
	_jeEllipse(_jeState.workingDC, x1, y1, x2, y2);
}

void jeDrawCircle(int x, int y, int r)
{
	jeDrawEllipse(x - r, y - r, x + r, y + r);
}

void jeDrawPolygon(POINT* points, int num)
{
	_jePolygon(_jeState.workingDC, points, num);
}

void jeSetPixel(int x, int y, COLORREF color)
{
	_jeSetPixel(_jeState.workingDC, x, y, color);
}

void jeDrawRectangle(int x1, int y1, int x2, int y2)
{
	_jeRectangle(_jeState.workingDC, x1, y1, x2, y2);
}

void jeDrawRoundRect(int x1, int y1, int x2, int y2, int roundX, int roundY)
{
	_jeRoundRect(_jeState.workingDC, x1, y1, x2, y2, roundX, roundY);
}

void jeInvertRect(int x1, int y1, int x2, int y2)
{
	RECT rect = { x1, y1, x2, y2 };
	_jeInvertRect(_jeState.workingDC, &rect);
}

void jeFloodFill(int x, int y)
{
	JE_FUNCTION_START(jeFloodFill);
	_jeExtFloodFill(_jeState.workingDC, x, y, jeGetPixelColor(x, y), FLOODFILLSURFACE);
}

void jeDrawText(int x, int y, const char* text)
{
	JE_FUNCTION_START(jeDrawText);
	_jeTextOutA(_jeState.workingDC, x, y, text, strlen(text));
}

void jeDrawArc(int x1, int y1, int x2, int y2, int xStart, int yStart, int xEnd, int yEnd, bool clockwise)
{
	JE_FUNCTION_START(jeDrawArc);
	if (clockwise)
		_jeSetArcDirection(_jeState.workingDC, AD_CLOCKWISE);
	else
		_jeSetArcDirection(_jeState.workingDC, AD_COUNTERCLOCKWISE);

	_jeArc(_jeState.workingDC, x1, y1, x2, y2, xStart, yStart, xEnd, yEnd);
}

void jeDrawChoord(int x1, int y1, int x2, int y2, int xStart, int yStart, int xEnd, int yEnd, bool clockwise)
{
	JE_FUNCTION_START(jeDrawChoord);
	if (clockwise)
		_jeSetArcDirection(_jeState.workingDC, AD_CLOCKWISE);
	else
		_jeSetArcDirection(_jeState.workingDC, AD_COUNTERCLOCKWISE);

	_jeChord(_jeState.workingDC, x1, y1, x2, y2, xStart, yStart, xEnd, yEnd);
}

void jeDrawPie(int x1, int y1, int x2, int y2, int xStart, int yStart, int xEnd, int yEnd, bool clockwise)
{
	JE_FUNCTION_START(jeDrawPie);
	if (clockwise)
		_jeSetArcDirection(_jeState.workingDC, AD_CLOCKWISE);
	else
		_jeSetArcDirection(_jeState.workingDC, AD_COUNTERCLOCKWISE);

	_jePie(_jeState.workingDC, x1, y1, x2, y2, xStart, yStart, xEnd, yEnd);
}

HDC jeLoadDC(const char* address)
{
	JE_FUNCTION_START(jeLoadDC);
	POINT size;
	return jeLoadDCEx(address, &size);
}

HDC jeLoadDCEx(const char* address, POINT* size)
{
	JE_FUNCTION_START(jeLoadDCEx);

	JE_ASSERT(address);
	JE_ASSERT(size);

	FILE* file = fopen(address, "rb");
	JE_ASSERT(file);

	fseek(file, sizeof (BITMAPFILEHEADER), SEEK_SET);

	BITMAPINFOHEADER bmpinfo;
	memset(&bmpinfo, 0, sizeof(BITMAPINFOHEADER));
	fread(&bmpinfo, sizeof (bmpinfo), 1, file);

	fclose (file);

	size->x = bmpinfo.biWidth;
	size->y = bmpinfo.biHeight;

	HBITMAP bitmap = (HBITMAP) LoadImage(NULL, address, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	JE_ASSERT(bitmap != NULL);

	HDC dc = _jeCreateCompatibleDC(_jeState.windowDC);
	JE_ASSERT(dc);

	_jeSelectObject(dc, bitmap);
	_jeSetBkMode(dc, TRANSPARENT);

	return dc;
}

HDC jeCreateDC(int sizex, int sizey)
{
	JE_FUNCTION_START(CreateNewDC);

	HDC dc = _jeCreateCompatibleDC(_jeState.windowDC);
	JE_ASSERT(dc);

	HBITMAP bitmap = _jeCreateCompatibleBitmap(_jeState.windowDC, sizex, sizey);
	JE_ASSERT(bitmap);

	_jeSelectObject(dc, bitmap);
	_jeSetBkMode(dc, TRANSPARENT);

	return dc;
}

void jeDeleteDC(HDC dc)
{
	JE_FUNCTION_START(jeDeleteDC);

	if (dc == _jeState.windowDC)
		jeError("Нельзя удалить HDC окна. Не волнуйтесь, в конце программы она выгрузится :)");
	if (dc == _jeState.workingDC)
		jeError("Нельзя удалить HDC, который сейчас текущий.");

	_jeDeleteDCWithoutCheck(dc);
}

void _jeDeleteDCWithoutCheck(HDC dc)
{
	_jeDeleteObject(_jeSelectObject(dc, _jeGetStockObject(NULL_PEN)));
	_jeDeleteObject(_jeSelectObject(dc, _jeGetStockObject(NULL_BRUSH)));
	_jeDeleteObject(_jeSelectObject(dc, _jeGetStockObject(SYSTEM_FONT)));

	_jeDeleteObject(_jeSelectObject(dc, _jeGetStockObject(OBJ_BITMAP)));
	_jeDeleteObject(_jeSelectObject(dc, _jeGetStockObject(OBJ_REGION)));

	_jeDeleteDC(dc);
}

void jeBitBlt(HDC dc, int x, int y, int sizex, int sizey,
			  int startx, int starty)
{
	JE_FUNCTION_START(jeBitBlt);
	_jeBitBlt(_jeState.workingDC, x, y, sizex, sizey, dc, startx, starty, SRCCOPY);
}

void jeTransparentBlt(COLORREF transparentColor, HDC dc, int x, int y, int sizex, int sizey,
					  int startx, int starty)
{
	JE_FUNCTION_START(jeTransparentBlt);
	_jeTransparentBlt(_jeState.workingDC, x, y, sizex, sizey, dc, startx, starty, sizex, sizey, transparentColor);
}

void jeAlphaBlend (BYTE transparent, HDC dc, int x, int y, int sizex, int sizey,
				   int startx, int starty, int zoomx, int zoomy)
{
	JE_FUNCTION_START(jeAlphaBlend);
	BLENDFUNCTION blend = { AC_SRC_OVER, 0, transparent, 0 };
	_jeAlphaBlend(_jeState.workingDC, x, y, zoomx, zoomy, dc, startx, starty, sizex, sizey, blend);
}

COLORREF jeGetPixelColor(int x, int y)
{
	JE_FUNCTION_START(jeGetPixelColor);
	return _jeGetPixel(_jeState.workingDC, x, y);
}

void jeSelectFont (char* name, int sizey, bool bold, bool italic, bool underline, bool strikeout)
{
	JE_FUNCTION_START(jeSelectFont);

	int boldSize = 0;
	if (!bold) boldSize = FW_NORMAL;
	else       boldSize = FW_BOLD;

	HFONT font = _jeCreateFontA(sizey, sizey / 3, 0, 0, boldSize, italic, underline, strikeout,
							    DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
							    DEFAULT_QUALITY, FIXED_PITCH, name);
	JE_ASSERT(font);

	_jeDeleteObject(_jeSelectObject(_jeState.workingDC, font));
}

void jeSelectRegion(int x1, int y1, int x2, int y2)
{
	JE_FUNCTION_START(jeSelectRegion);

	HRGN region = _jeCreateRectRgn(x1, y1, x2, y2);
	_jeSelectObject(_jeState.workingDC, region);
	_jeDeleteObject(region);
}

void jeSelectNormalRegion()
{
	JE_FUNCTION_START(jeSelectRegion);
	if (_jeState.windowDC != _jeState.workingDC)
		jeError("Нельзя вызвать функцию jeSelectNormalRegion не для HDC окна.");
	jeSelectRegion(0, 0, _jeState.windowSizeX, _jeState.windowSizeY);
}

HDC jeGetWindowDC()
{
	JE_FUNCTION_START(jeGetWindowDC);
	return _jeState.windowDC;
}

HWND jeGetWindow()
{
	JE_FUNCTION_START(jeGetWindow);
	return _jeState.window;
}

void jeSetWorkingDC(HDC dc)
{
	JE_FUNCTION_START(jeSetWorkingDC);
	_jeState.workingDC = dc;
}

void jeSetMainDCWorking()
{
	JE_FUNCTION_START(jeSetMainDCWorking);
	_jeState.workingDC = _jeState.windowDC;
}

BYTE jeGetColorR(COLORREF color)
{
	JE_FUNCTION_START(jeGetColorR);
	return GetRValue(color);
}

BYTE jeGetColorG(COLORREF color)
{
	JE_FUNCTION_START(jeGetColorG);
	return GetGValue(color);
}

BYTE jeGetColorB(COLORREF color)
{
	JE_FUNCTION_START(jeGetColorB);
	return GetBValue(color);
}

void jeSleep(int time)
{
	JE_FUNCTION_START(jeSleep);
	Sleep(time);
}

POINT jeWindowSize()
{
	JE_FUNCTION_START(jeWindowSize);
	POINT size = { _jeState.windowSizeX, _jeState.windowSizeY };
	return size;
}

int jeWindowSizeX()
{
	JE_FUNCTION_START(jeWindowSizeX);
	return _jeState.windowSizeX;
}

int jeWindowSizeY()
{
	JE_FUNCTION_START(jeWindowSizeY);
	return _jeState.windowSizeY;
}

POINT jeGetTextSize(const char* text)
{
	JE_FUNCTION_START(jeGetTextSize);

	SIZE textSize;
	_jeGetTextExtentPoint32A(_jeState.workingDC, text, strlen(text), &textSize);

	POINT textSizePoint = { textSize.cx, textSize.cy };
	return textSizePoint;
}

int jeGetTextSizeX(const char* text)
{
	JE_FUNCTION_START(jeGetTextSizeX);
	return jeGetTextSize(text).x;
}

int jeGetTextSizeY(const char* text)
{
	JE_FUNCTION_START(jeGetTextSizeY);
	return jeGetTextSize(text).y;
}

bool jeIsKeyPressed(int code)
{
	JE_FUNCTION_START(jeIsKeyPressed);
	JE_ASSERT(code >= 0 && code < JE_NUM_KEY_CODES);
	EnterCriticalSection(&_jeState.inputCriticalSection);
	bool pressed = _jeState.keys[code];
	LeaveCriticalSection(&_jeState.inputCriticalSection);
	return pressed;
}

bool jeLButtonPressed()
{
	JE_FUNCTION_START(jeLButtonPressed);
	EnterCriticalSection(&_jeState.inputCriticalSection);
	bool pressed = _jeState.keys[VK_LBUTTON];
	LeaveCriticalSection(&_jeState.inputCriticalSection);
	return pressed;
}

bool jeRButtonPressed()
{
	JE_FUNCTION_START(jeRButtonPressed);
	EnterCriticalSection(&_jeState.inputCriticalSection);
	bool pressed = _jeState.keys[VK_RBUTTON];
	LeaveCriticalSection(&_jeState.inputCriticalSection);
	return pressed;
}

POINT jeGetMousePosition()
{
	EnterCriticalSection(&_jeState.inputCriticalSection);
	JE_FUNCTION_START(jeGetMousePosition);
	POINT position = { _jeState.mouseX, _jeState.mouseY };
	LeaveCriticalSection(&_jeState.inputCriticalSection);
	return position;
}

int jeGetMouseX()
{
	JE_FUNCTION_START(jeGetMouseX);
	return _jeState.mouseX;
}

int jeGetMouseY()
{
	JE_FUNCTION_START(jeGetMouseY);
	return _jeState.mouseY;
}

void jeShowConsole()
{
	JE_FUNCTION_START(jeShowConsole);
	ShowWindow(_jeGetConsoleWindow(), SW_SHOWNOACTIVATE);
}

void jeHideConsole()
{
	JE_FUNCTION_START(jeHideConsole);
	ShowWindow(_jeGetConsoleWindow(), SW_HIDE);
}

void jeShowCursor()
{
	JE_FUNCTION_START(jeShowCursor);
	while (ShowCursor(true) < 0);
}

void jeHideCursor()
{
	JE_FUNCTION_START(jeShowCursor);
	while (ShowCursor(false) >= 0);
}

//=============================================================================

void jeError(const char* description)
{
	wchar_t wtext[JE_ERROR_MESSAGE_SIZE] = L"";
	const char* title = "Ошибка!";
	wchar_t wtitle[JE_ERROR_MESSAGE_SIZE] = L"";
	MultiByteToWideChar(CP_UTF8, 0, title, -1, wtitle, JE_ERROR_MESSAGE_SIZE);
	MultiByteToWideChar(CP_UTF8, 0, description, -1, wtext, JE_ERROR_MESSAGE_SIZE);
	WriteConsoleW(GetStdHandle(STD_OUTPUT_HANDLE), wtext, wcslen(wtext), NULL, NULL);
	MessageBoxW(NULL, wtext, wtitle, MB_ICONERROR);
	_jeFreeDataAndExit(true);
}

void _jeGetModuleFileName(char* name)
{
	static char path[MAX_PATH];

	if (!GetModuleFileName(NULL, path, sizeof (path) - 1))
		*path = 0;

	char* fileName = strrchr(path, '\\');
	if (!fileName)
		fileName = path;
	else
		fileName += 1;

	char* ext = strrchr(path, '.');
	int namelen = ext ? ext - fileName : (int) strlen(fileName);

	fileName[namelen] = 0;
	strcpy(name, fileName);
}

FARPROC _jeDllImport(const char* dllName, const char* funcName)
{
	HMODULE dll = GetModuleHandle(dllName);
	if (!dll)
		dll = LoadLibrary(dllName);
	JE_ASSERT(dll);

	FARPROC func = GetProcAddress (dll, funcName);
	JE_ASSERT(func);

	return func;
}

void _jeFreeDataAndExit(bool error)
{
	if (_jeState.initialized)
	{
		_jeDeleteDCWithoutCheck(_jeState.windowDC);
		_jeDeleteDCWithoutCheck(_jeState.readyWindowDC);
		
		DeleteCriticalSection(&_jeState.inputCriticalSection);
		DeleteCriticalSection(&_jeState.dcCriticalSection);
		// ^ it's not truely thread safe. If main thread
		// try to enter critical section, then state of main
		// thread is undefined but now we call exit(0)
		// so it's shoudn't be a big problem.
	}
	system("taskkill -im cb_console_runner.exe"); // hack for CodeBlocks
	// ^ without this string app without console will be still alive
	exit(error ? 255 : 0);
}

LRESULT CALLBACK _jeWndProc(HWND wnd, UINT msg, WPARAM wpar, LPARAM lpar)
{
	JE_FUNCTION_START(_jeWndProc);

	HDC dc = NULL;
	PAINTSTRUCT ps;

	switch (msg)
	{
		case WM_PAINT:
			{
				RECT rect;
				GetClientRect(wnd, &rect);
				dc = BeginPaint(wnd, &ps);

				EnterCriticalSection(&_jeState.dcCriticalSection);
				_jeBitBlt(dc, 0, 0, _jeState.windowSizeX, _jeState.windowSizeY, _jeState.readyWindowDC, 0, 0, SRCCOPY);
				LeaveCriticalSection(&_jeState.dcCriticalSection);
				EndPaint (wnd, &ps);
			}
			break;

		case WM_KEYDOWN:
			EnterCriticalSection(&_jeState.inputCriticalSection);
			_jeState.keys[wpar] = true;
			LeaveCriticalSection(&_jeState.inputCriticalSection);
			break;

		case WM_KEYUP:
			EnterCriticalSection(&_jeState.inputCriticalSection);
			_jeState.keys[wpar] = false;
			LeaveCriticalSection(&_jeState.inputCriticalSection);
			break;

		case WM_LBUTTONDOWN:
			EnterCriticalSection(&_jeState.inputCriticalSection);
			_jeState.keys[VK_LBUTTON] = true;
			LeaveCriticalSection(&_jeState.inputCriticalSection);
			break;

		case WM_LBUTTONUP:
			EnterCriticalSection(&_jeState.inputCriticalSection);
			_jeState.keys[VK_LBUTTON] = false;
			LeaveCriticalSection(&_jeState.inputCriticalSection);
			break;

		case WM_RBUTTONDOWN:
			EnterCriticalSection(&_jeState.inputCriticalSection);
			_jeState.keys[VK_RBUTTON] = true;
			LeaveCriticalSection(&_jeState.inputCriticalSection);
			break;

		case WM_RBUTTONUP:
			EnterCriticalSection(&_jeState.inputCriticalSection);
			_jeState.keys[VK_RBUTTON] = false;
			LeaveCriticalSection(&_jeState.inputCriticalSection);
			break;

		case WM_MOUSEMOVE:
			{
				RECT rect;
				GetClientRect(wnd, &rect);

				EnterCriticalSection(&_jeState.inputCriticalSection);
				_jeState.mouseX = LOWORD(lpar);
				_jeState.mouseY = HIWORD(lpar);
				LeaveCriticalSection(&_jeState.inputCriticalSection);
			}
			break;

		case WM_CLOSE:
			DestroyWindow(wnd);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			_jeFreeDataAndExit(false);
			break;

		default:
			return DefWindowProc(wnd, msg, wpar, lpar);
			break;
	}

	return 0;
}

#if defined (_MSC_VER) && (_MSC_VER >= 1400) // MSVC 8 (2005) or greater
	#pragma warning (default: 4996)
#endif

#endif // JElib.c

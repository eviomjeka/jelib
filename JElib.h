//! \file    JElib.h
//! \brief   Main header JElib file

#ifndef JELIB_H
#define JELIB_H

#include <Windows.h>
#include <stdio.h>
#include <math.h>
#include <io.h>
#include <time.h>
#include <locale.h>
#ifndef _MSC_VER
	#include <stdbool.h>
#elif !defined __cplusplus
	#define bool int
	#define true 1
	#define false 0
#endif

#define JE_VERSION     1.11

#define VK_ALT   VK_MENU
#define VK_ENTER VK_RETURN

#ifdef __FUNCSIG__
#define JE_FUNCTION __FUNCSIG__
#elif  __PRETTY_FUNCTION__
#define JE_FUNCTION __PRETTY_FUNCTION__
#else
#define JE_FUNCTION __FUNCTION__
#endif

#define JE_NUM_KEY_CODES 256
#define JE_PI 3.1415926535

typedef struct
{
	bool initialized;
	
	HWND window;
	HDC windowDC;
	HDC workingDC;
	int windowSizeX;
	int windowSizeY;
	
	CRITICAL_SECTION dcCriticalSection;
	HDC readyWindowDC;
	
	CRITICAL_SECTION inputCriticalSection;
	int mouseX;
	int mouseY;
	bool keys[JE_NUM_KEY_CODES];
	
	COLORREF color;
	COLORREF fillColor;
	int lineWidth;
} _jeState_t;

enum JE_COLORS
{
	JE_RED         = RGB(255, 0, 0),
	JE_GREEN       = RGB(0, 255, 0),
	JE_BLUE        = RGB(0, 0, 255),
	JE_YELLOW      = RGB(255, 255, 0),
	JE_PURPLE      = RGB(255, 0, 255),
	JE_CYAN        = RGB(0, 255, 255),
	JE_WHITE       = RGB(255, 255, 255),
	JE_BLACK       = RGB(0, 0, 0),
	JE_ORANGE      = RGB(255, 128, 64),
	JE_BROWN       = RGB(128, 64, 0),
	JE_VIOLET      = RGB(128, 0, 255),
	JE_GREY        = RGB(200, 200, 200),
	JE_DARK_GREY   = RGB(100, 100, 100),
	JE_TRANSPARENT = 0xFFFFFFFF
};

/** \brief			Создает окно для рисования заданного размера.
  *
  * \param	width	Ширина окна
  * \param	height	Высота окна
  *
  * \see			jeCreateWindowEx()
  * 
  * \usage \code
  *		jeCreateWindow(800, 600);
  *		jeDrawCircle(400, 300, 100);
  *		jeUpdate();
  * \endcode
  */
void jeCreateWindow(int width, int height);

/** \brief			Создает окно для рисования заданного размера и с указанным заголовком.
  *
  * \param	width	Ширина окна
  * \param	height	Высота окна
  * \param	name	Заголовок окна
  *
  * \see			jeCreateWindow()
  * 
  * \usage \code
  *		jeCreateWindowEx(800, 600, "My first window");
  *		jeDrawLine(0, 0, 800, 600);
  *		jeUpdate();
  * \endcode
  */
void jeCreateWindowEx(int width, int height, const char* name);

/** \brief			Посылает получившееся избражение на отображение (применяются последние измененения после предыдущего вызова).
  * 
  * \usage \code
  *		jeDrawLine(0, 0, 10, 10);
  *		jeUpdate();
  *		jeSleep(2000);
  *		jeDrawLine(10, 0, 0, 10);
  *		jeUpdate();
  * \endcode
  */
void jeUpdate();

/** \brief			Завершает программу, оставляя окно открытым.
  * 
  * \usage \code
  *		int main()
  *		{
  *			jeCreateWindow(800, 600);
  *			jeDrawLine(0, 0, 800, 600);
  *			jeUpdate();
  *			jeWait();
  *			return 0;
  *		}
  * \endcode
  */
void jeWait();

/** \brief			Устанавливает цвет контура.
  * 
  * \param	color	Новый цвет контура
  *
  * \see			jeSetLineWidth(), jeSetColorEx(), jeSetFillColor(), jeGetColor(), jeGetFillColor()
  * 
  * \usage \code
  *		jeSetColor(JE_RED);
  *		jeDrawCircle(10, 10, 5);
  * \endcode
  */
void jeSetColor     (COLORREF color);

/** \brief			Устанавливает ширину контура.
  * 
  * \param	size	Новая ширина контура
  *
  * \see			jeSetColor(), jeSetColorEx(), jeSetFillColor(), jeGetColor(), jeGetFillColor()
  * 
  * \usage \code
  *		jeSetLineWidth(8);
  *		jeDrawEllipse(0, 0, 200, 400);
  * \endcode
  */
void jeSetLineWidth (int size);

/** \brief			Устанавливает цвет и ширину контура.
  * 
  * \param	color	Новый цвет контура
  * \param	size	Новая ширина контура
  *
  * \see			jeSetColor(), jeSetLineWidth(), jeSetFillColor(), jeGetColor(), jeGetFillColor()
  * 
  * \usage \code
  *		jeSetColorEx(JE_BLUE, 6);
  *		jeDrawRectangle(0, 0, 200, 300);
  * \endcode
  */
void jeSetColorEx   (COLORREF color, int size);

/** \brief			Устанавливает цвет заливкм.
  * 
  * \param	color	Новый цвет заливки
  *
  * \see			jeSetColor(), jeSetLineWidth(), jeSetColorEx(), jeGetColor(), jeGetFillColor()
  * 
  * \usage \code
  *		jeSetFillColor(JE_ORANGE);
  *		jeDrawRoundRect(0, 0, 500, 200);
  * \endcode
  */
void jeSetFillColor (COLORREF color);

/** \brief			Получить цвет контура.
  *
  * \return			Текущий цвет контура
  *
  * \see			jeSetColor(), jeSetLineWidth(), jeSetColorEx(), jeSetFillColor(), jeGetFillColor()
  * 
  * \usage \code
  *		jeSetColor(JE_GREEN);
  *		jeSetFillColor(jeGetColor());
  *		jeDrawRectangle(0, 0, 300, 100);
  * \endcode
  */
COLORREF jeGetColor();

/** \brief			Получить цвет заливки.
  *
  * \return			Текущий цвет заливки
  *
  * \see			jeSetColor(), jeSetLineWidth(), jeSetColorEx(), jeSetFillColor(), jeGetColor()
  * 
  * \usage \code
  *		jeSetFillColor(JE_VIOLET);
  *		jeDrawCircle(10, 10, 5);
  *		COLORREF color = jeGetFillColor();
  *		jeSetFillColor(JE_BROWN);
  *		jeDrawCircle(20, 10, 5);
  *		jeSetFillColor(color);
  *		jeDrawCircle(30, 10, 5);
  * \endcode
  */
COLORREF jeGetFillColor();

/** \brief			Рисует отрезок между указанными точками.
  * 
  * \param	x1		x-координата первой точки
  * \param	y1		y-координата первой точки
  * \param	x2		x-координата второй точки
  * \param	y2		y-координата второй точки
  *
  * \see			jeDrawLine(), jeDrawEllipse(), jeDrawCircle(), jeDrawPolygon(), jeSetPixel(), jeDrawRectangle(), jeDrawRoundRect(),
  *					jeInvertRect(), jeFloodFill(), jeDrawText(), jeDrawArc(), jeDrawChoord(), jeDrawPie(), jeDrawPolygon(), jeSetPixel()
  * 
  * \usage \code
  * 	jeSetColorEx(JE_YELLOW, 4);
  * 	jeDrawLine(10, 10, 10, 25);
  * \endcode
  */
void jeDrawLine       (int x1, int y1, int x2, int y2);

/** \brief			Рисует эллипс, вписанный в прямоугольник, заданный противоположными вершинами.
  * 
  * \param	x1		x-координата первой вершины
  * \param	y1		y-координата первой вершины
  * \param	x2		x-координата второй вершины
  * \param	y2		y-координата второй вершины
  *
  * \see			jeDrawLine(), jeDrawEllipse(), jeDrawCircle(), jeDrawPolygon(), jeSetPixel(), jeDrawRectangle(), jeDrawRoundRect(),
  *					jeInvertRect(), jeFloodFill(), jeDrawText(), jeDrawArc(), jeDrawChoord(), jeDrawPie(), jeDrawPolygon(), jeSetPixel()
  * 
  * \usage \code
  * 	jeSetFillColor(JE_BLACK);
  * 	jeDrawRectangle(0, 0, 200, 100);
  * 	jeSetFillColor(JE_WHITE);
  * 	jeDrawEllipse(0, 0, 200, 100);
  * \endcode
  */
void jeDrawEllipse    (int x1, int y1, int x2, int y2);

/** \brief			Рисует круг, заданный координатами центра и радиусом.
  * 
  * \param	x		x-координата центра
  * \param	y		y-координата центра
  * \param	r		Радиус
  *
  * \see			jeDrawLine(), jeDrawEllipse(), jeDrawCircle(), jeDrawPolygon(), jeSetPixel(), jeDrawRectangle(), jeDrawRoundRect(),
  *					jeInvertRect(), jeFloodFill(), jeDrawText(), jeDrawArc(), jeDrawChoord(), jeDrawPie(), jeDrawPolygon(), jeSetPixel()
  * 
  * \usage \code
  * 	jeDrawEllipse(0, 0, 10, 10);
  * 	jeDrawCircle(15, 15, 5);
  * \endcode
  */
void jeDrawCircle     (int x, int y, int r);

/** \brief			Рисует прямоугольник, заданный противоположными вершинами.
  * 
  * \param	x1		x-координата первой вершины
  * \param	y1		y-координата первой вершины
  * \param	x2		x-координата второй вершины
  * \param	y2		y-координата второй вершины
  *
  * \see			jeDrawLine(), jeDrawEllipse(), jeDrawCircle(), jeDrawPolygon(), jeSetPixel(), jeDrawRectangle(), jeDrawRoundRect(),
  *					jeInvertRect(), jeFloodFill(), jeDrawText(), jeDrawArc(), jeDrawChoord(), jeDrawPie(), jeDrawPolygon(), jeSetPixel()
  * 
  * \usage \code
  * 	jeSetFillColor(JE_RED);
  * 	jeDrawRectangle(0, 0, 800, 600);
  * 	jeSetFillColor(JE_BLUE);
  * 	jeDrawRectangle(0, 0, 400, 300);
  * \endcode
  */
void jeDrawRectangle  (int x1, int y1, int x2, int y2);

/** \brief			Рисует прямоугольник, заданный противоположными вершинами c эллиптически закругленными краями.
  * 
  * \param	x1		x-координата первой вершины
  * \param	y1		y-координата первой вершины
  * \param	x2		x-координата второй вершины
  * \param	y2		y-координата второй вершины
  * \param	roundX	"Ширина" закругляющего эллипса
  * \param	roundY	"Высота" закругляющего эллипса
  *
  * \see			jeDrawLine(), jeDrawEllipse(), jeDrawCircle(), jeDrawPolygon(), jeSetPixel(), jeDrawRectangle(), jeDrawRoundRect(),
  *					jeInvertRect(), jeFloodFill(), jeDrawText(), jeDrawArc(), jeDrawChoord(), jeDrawPie(), jeDrawPolygon(), jeSetPixel()
  * 
  * \usage \code
  *		jeSetFillColor(JE_DARK_GREY);
  * 	jeDrawRectangle(0, 0, 800, 600);
  * 	jeSetFillColor(JE_GREY);
  * 	jeDrawRoundRect(0, 0, 800, 600, 20, 20);
  * \endcode
  */
void jeDrawRoundRect  (int x1, int y1, int x2, int y2, int roundX, int roundY);

/** \brief			Инвертирует цвет в прямоугольнике, заданном противоположными вершинами.
  * 
  * \param	x1		x-координата первой вершины
  * \param	y1		y-координата первой вершины
  * \param	x2		x-координата второй вершины
  * \param	y2		y-координата второй вершины
  *
  * \see			jeDrawLine(), jeDrawEllipse(), jeDrawCircle(), jeDrawPolygon(), jeSetPixel(), jeDrawRectangle(), jeDrawRoundRect(),
  *					jeInvertRect(), jeFloodFill(), jeDrawText(), jeDrawArc(), jeDrawChoord(), jeDrawPie(), jeDrawPolygon(), jeSetPixel()
  * 
  * \usage \code
  * 	jeSetFillColor(JE_ORANGE);
  * 	jeDrawRectangle(0, 0, 800, 600);
  *		jeInvertRect(200, 150, 600, 450);
  * \endcode
  */
void jeInvertRect     (int x1, int y1, int x2, int y2);

/** \brief			Работает как заливка в Paint. Рекомендуется вызывать в очень редких случаях, так
  *					как работает медленно.
  * 
  * \param	x		x-координата точки
  * \param	y		y-координата точки
  *
  * \see			jeDrawLine(), jeDrawEllipse(), jeDrawCircle(), jeDrawPolygon(), jeSetPixel(), jeDrawRectangle(), jeDrawRoundRect(),
  *					jeInvertRect(), jeFloodFill(), jeDrawText(), jeDrawArc(), jeDrawChoord(), jeDrawPie(), jeDrawPolygon(), jeSetPixel()
  * 
  * \usage \code
  * 	jeSetFillColor(JE_GREEN);
  * 	jeDrawRectangle(0, 0, 800, 600);
  *		jeInvertRect(200, 150, 600, 450);
  * 	jeSetFillColor(JE_VIOLET);
  *		jeFloodFill(0, 0);
  * \endcode
  */
void jeFloodFill      (int x, int y);

/** \brief			Рисует текст в заданных координатах.
  * 
  * \param	x		x-координата начальной точки
  * \param	y		y-координата начальной точки
  *
  * \see			jeDrawLine(), jeDrawEllipse(), jeDrawCircle(), jeDrawPolygon(), jeSetPixel(), jeDrawRectangle(), jeDrawRoundRect(),
  *					jeInvertRect(), jeFloodFill(), jeDrawText(), jeDrawArc(), jeDrawChoord(), jeDrawPie(), jeDrawPolygon(), jeSetPixel()
  * 
  * \usage \code
  * 	jeSetFillColor(JE_RED);
  *		jeDrawText(10, 10, "Hello world!");
  * \endcode
  */
void jeDrawText       (int x, int y, const char* text);

void jeDrawArc       (int x1, int y1, int x2, int y2, int xStart, int yStart, int xEnd, int yEnd, bool clockwise);
void jeDrawChoord    (int x1, int y1, int x2, int y2, int xStart, int yStart, int xEnd, int yEnd, bool clockwise);
void jeDrawPie       (int x1, int y1, int x2, int y2, int xStart, int yStart, int xEnd, int yEnd, bool clockwise);

/** \brief			Рисует многоугольник. Вершины многоугольника хранятся в points. Количество вершин хранится в num.
  * 
  * \param	points	массив вершин многоугольника
  * \param	y		количество вершин многоугольника
  *
  * \see			jeDrawLine(), jeDrawEllipse(), jeDrawCircle(), jeDrawPolygon(), jeSetPixel(), jeDrawRectangle(), jeDrawRoundRect(),
  *					jeInvertRect(), jeFloodFill(), jeDrawText(), jeDrawArc(), jeDrawChoord(), jeDrawPie(), jeDrawPolygon(), jeSetPixel()
  *
  * \usage \code
  *		POINT a = { 10, 10 };
  *		POINT b = { 100, 10 };
  *		POINT c = { 10, 100 };
  *		POINT points[3] = { a, b, c };
  *		jeDrawPolygon(points, 3);
  * \endcode
  */
void jeDrawPolygon(POINT* points, int num);


/** \brief			Устанавливает пиксель в заданный цвет.
  * 
  * \param	x		x-координата пикселя
  * \param	y		y-координата пикселя
  * \param	color	Новый цвет пикселя
  *
  * \see			jeDrawLine(), jeDrawEllipse(), jeDrawCircle(), jeDrawPolygon(), jeSetPixel(), jeDrawRectangle(), jeDrawRoundRect(),
  *					jeInvertRect(), jeFloodFill(), jeDrawText(), jeDrawArc(), jeDrawChoord(), jeDrawPie(), jeDrawPolygon(), jeGetPixelColor()
  * 
  * \usage \code
  * 	jeSetFillColor(JE_YELLOW);
  * 	jeDrawRectangle(0, 0, 800, 600);
  *		jeSetPixel(400, 300, JE_VIOLET);
  * \endcode
  */
void jeSetPixel       (int x, int y, COLORREF color);
/** \brief			Возвращает цвет пикселя в заданных координатах.
  * 
  * \param	x		x-координата пикселя
  * \param	y		y-координата пикселя
  *
  * \return			Цвет пикселя
  *
  * \see			jeSetPixel()
  * 
  * \usage \code
  * 	jeSetFillColor(JE_YELLOW);
  * 	jeDrawRectangle(0, 0, 800, 600);
  *		jeDrawCircle(10, 10, 5);
  *		jeSetPixel(400, 300, jeGetPixelColor(10, 10));
  * \endcode
  */
COLORREF jeGetPixelColor (int x, int y);

/** \brief			Загружает картинку в HDC из файла. Принимается только BMP-файлы. Не забудьте удалить HDC
  *	                с помощью функции @ref jeDeleteDC.
  * 
  * \param	address	адрес BMP-картинки
  *
  * \return			HDC картинки из файла
  *
  * \see			jeLoadDCEx(), jeCreateDC(), jeDeleteDC(), jeBitBlt(), jeTransparentBlt(), jeAlphaBlend()
  * 
  * \usage \code
  *		POINT size = {};
  * 	HDC dc = jeLoadDCEx("file.bmp", &size);
  *		while(true)
  *		{
  *			jeBitBlt(dc, x, y, size.x, size.y, 0, 0);
  *			jeUpdate();
  *			jeSleep(10);
  *		}
  *		jeDeleteDC(dc);
  * \endcode
  */
HDC jeLoadDC(const char* address);
/** \brief			Загружает картинку в HDC из файла. Принимается только BMP-файлы. Не забудьте удалить HDC
  *	                с помощью функции @ref jeDeleteDC. Отличается от функции @ref jeLoadDC тем, что в второй
  *					параметр (size) записывает размер картинки.
  * 
  * \param	address	адрес BMP-картинки
  * \param	size	сюда запишется размер картинки
  *
  * \return			HDC картинки из файла
  *
  * \see			jeLoadDCEx(), jeCreateDC(), jeDeleteDC(), jeBitBlt(), jeTransparentBlt(), jeAlphaBlend()
  * 
  * \usage \code
  * 	HDC dc = jeLoadDC("file.bmp");
  *		while(true)
  *		{
  *			jeBitBlt(dc, x, y, 50, 50, 0, 0);
  *			jeUpdate();
  *			jeSleep(10);
  *		}
  *		jeDeleteDC(dc);
  * \endcode
  */
HDC jeLoadDCEx(const char* address, POINT* size);
HDC jeCreateDC(int sizex, int sizey);
/** \brief			Удаляет HDC. Эту функцию надо вызывать для каждой открытой HDC, иначе у вас на
  *					компьютере будет утечка памяти. После вызова этой функции HDC уже нельзя будет
  *					использовать.
  * 
  * \param	dc		HDC, которая будет удалена
  *
  * \see			jeLoadDCEx(), jeCreateDC(), jeDeleteDC(), jeBitBlt(), jeTransparentBlt(), jeAlphaBlend()
  * 
  * \usage \code
  *		POINT size = {};
  * 	HDC dc = jeLoadDCEx("file.bmp", &size);
  *		while(true)
  *		{
  *			jeBitBlt(dc, x, y, size.x, size.y, 0, 0);
  *			jeUpdate();
  *			jeSleep(10);
  *		}
  *		jeDeleteDC(dc);
  * \endcode
  */
void jeDeleteDC(HDC dc);

/** \brief			Рисует HDC.
  * 
  * \param	dc		HDC, которая будет рисоваться
  * \param	x		x-координата, где будет рисоваться картинка
  * \param	y		y-координата, где будет рисоваться картинка
  * \param	sizex	длина той области картинки, которая будет рисоваться (если хотите нарисовать всю картинку,
					то укажите длину каринки)
  * \param	sizey	ширина той области картинки, которая будет рисоваться (если хотите нарисовать всю картинку,
					то укажите ширину каринки)
  * \param	startx	с какой x-координаты надо начать рисовать внтури картинку (если хотите нарисовать всю
  *					картинку, то укажите 0)
  * \param	startx	с какой y-координаты надо начать рисовать внтури картинку (если хотите нарисовать всю
  *					картинку, то укажите 0)
  *
  * \see			jeLoadDCEx(), jeCreateDC(), jeDeleteDC(), jeBitBlt(), jeTransparentBlt(), jeAlphaBlend()
  * 
  * \usage \code
  * 	HDC dc = jeLoadDC("file.bmp", &size);
  *		while(true)
  *		{
  *			jeBitBlt(dc, x, y, 50, 50, 0, 0);
  *			jeUpdate();
  *			jeSleep(10);
  *		}
  *		jeDeleteDC(dc);
  * \endcode
  */
void jeBitBlt(HDC dc, int x, int y, int sizex, int sizey, int startx, int starty);

/** \brief			Рисует HDC со следующей особенностью: все пиксели картинки с цветом transparentColor
  *					не будут рисоваться (то есть картинка будет прозрачной в этих местах).
  * 
  * \param  transparentColor	цвет, который не будет рисоваться
  * \param	dc		HDC, которая будет рисоваться
  * \param	x		x-координата, где будет рисоваться картинка
  * \param	y		y-координата, где будет рисоваться картинка
  * \param	sizex	длина той области картинки, которая будет рисоваться (если хотите нарисовать всю картинку,
					то укажите длину каринки)
  * \param	sizey	ширина той области картинки, которая будет рисоваться (если хотите нарисовать всю картинку,
					то укажите ширину каринки)
  * \param	startx	с какой x-координаты надо начать рисовать внтури картинку (если хотите нарисовать всю
  *					картинку, то укажите 0)
  * \param	startx	с какой y-координаты надо начать рисовать внтури картинку (если хотите нарисовать всю
  *					картинку, то укажите 0)
  *
  * \see			jeLoadDCEx(), jeCreateDC(), jeDeleteDC(), jeBitBlt(), jeTransparentBlt(), jeAlphaBlend()
  * 
  * \usage \code
  * 	HDC dc = jeLoadDC("file.bmp", &size);
  *		while(true)
  *		{
  *			jeTransparentBlt(JE_WHITE, dc, x, y, 50, 50, 0, 0);
  *			jeUpdate();
  *			jeSleep(10);
  *		}
  *		jeDeleteDC(dc);
  * \endcode
  */
void jeTransparentBlt(COLORREF transparentColor, HDC dc, int x, int y, int sizex, int sizey, int startx, int starty);

/** \brief			Рисует HDC с полупрозрачностью. Также позволяет растягивать картинку.
  * 
  * \param  transparent	прозрачность картинки. 255 - абсолютно непрозрачна (аналогична вызову @ref jeBitBlt), 
  *						0 - ничего не рисовать
  * \param	dc		HDC, которая будет рисоваться
  * \param	x		x-координата, где будет рисоваться картинка
  * \param	y		y-координата, где будет рисоваться картинка
  * \param	sizex	длина той области картинки, которая будет рисоваться (если хотите нарисовать всю картинку,
					то укажите длину каринки)
  * \param	sizey	ширина той области картинки, которая будет рисоваться (если хотите нарисовать всю картинку,
					то укажите ширину каринки)
  * \param	startx	с какой x-координаты надо начать рисовать внтури картинку (если хотите нарисовать всю
  *					картинку, то укажите 0)
  * \param	startx	с какой y-координаты надо начать рисовать внтури картинку (если хотите нарисовать всю
  *					картинку, то укажите 0)
  * \param zoomx	какого размера нарисовать область [startx, startx + sizex] (если написать sizex, то
  *					расстягивание по x-координате не будет)
  * \param zoomy	какого размера нарисовать область [starty, starty + sizey] (если написать sizey, то
  *					расстягивание по x-координате не будет)
  *
  * \see			jeLoadDCEx(), jeCreateDC(), jeDeleteDC(), jeBitBlt(), jeTransparentBlt(), jeAlphaBlend()
  * 
  * \usage \code
  * 	HDC dc = jeLoadDC("file.bmp", &size);
  *		while(true)
  *		{
  *			jeAlphaBlend(200, dc, x, y, 50, 50, 0, 0, 100, 100); // картинка будет увеличена в 2 раза
  *			jeUpdate();
  *			jeSleep(10);
  *		}
  *		jeDeleteDC(dc);
  * \endcode
  */
void jeAlphaBlend(BYTE transparent, HDC dc, int x, int y, int sizex, int sizey, int startx, int starty, int zoomx, int zoomy);


/** \brief				Устанавливает текущий шрифт.
  * 
  * \param	name		Название нового шрифта
  *	\param	sizey		Высота буков
  * \param	bold		Жирный ли текст
  * \param	italic		Курсивный ли текст
  * \param	underline	Подчеркнутый ли текст
  * \param	strikeout	Зачеркнутый ли текст
  *
  * \see				jeDrawText()
  * 
  * \usage \code
  * 	jeSelectFont("Arial", 10, true, false, false, false);
  *		jeDrawText(0, 0, "YES");
  * 	jeSelectFont("Comic Sans MS", 10, false, false, false, true);
  *		jeDrawText(0, 10, "NO");
  * \endcode
  */
void jeSelectFont (char* name, int sizey, bool bold, bool italic, bool underline, bool strikeout);

/** \brief			Запрещает изменения вне прямоугольника, заданного противоположными вершинами.
  * 
  * \param	x1		x-координата первой вершины
  * \param	y1		y-координата первой вершины
  * \param	x2		x-координата второй вершины
  * \param	y2		y-координата второй вершины
  *
  * \see			jeSelectNormalRegion()
  * 
  * \usage \code
  *		jeSelectRegion(200, 150, 600, 450);
  * 	jeDrawRectangle(0, 0, 800, 600);
  * \endcode
  */
void jeSelectRegion (int x1, int y1, int x2, int y2);

/** \brief			Разрешает изменения на всем холсте (отмена всех jeSelectRegion()).
  *
  * \see			jeSelectRegion()
  * 
  * \usage \code
  *		jeSelectRegion(200, 150, 600, 450);
  *		jeSelectNormalRegion();
  * 	jeDrawRectangle(0, 0, 800, 600);
  * \endcode
  */
void jeSelectNormalRegion();

HDC  jeGetWindowDC();
HWND jeGetWindow();

void jeSetWorkingDC(HDC dc);
void jeSetMainDCWorking();


/** \brief			Возвращает величину красной компоненты данного цвета
  * 
  * \param	color	цвет
  *
  * \return			величина красной компоненты данного цвета
  *
  * \see			jeGetColorG(), jeGetColorB()
  * 
  * \usage \code
  *		printf("%d\n", jeGetColorR(JE_RED);
  * \endcode
  */
BYTE jeGetColorR (COLORREF color);

/** \brief			Возвращает величину зелёной компоненты данного цвета
  * 
  * \param	color	цвет
  *
  * \return			величина зелёной компоненты данного цвета
  *
  * \see			jeGetColorR(), jeGetColorB()
  * 
  * \usage \code
  *		printf("%d\n", jeGetColorR(JE_VIOLET);
  * \endcode
  */
BYTE jeGetColorG (COLORREF color);

/** \brief			Возвращает величину синей компоненты данного цвета
  * 
  * \param	color	цвет
  *
  * \return			величина синей компоненты данного цвета
  *
  * \see			jeGetColorR(), jeGetColorG()
  * 
  * \usage \code
  *		printf("%d\n", jeGetColorR(JE_YELLOW);
  * \endcode
  */
BYTE jeGetColorB (COLORREF color);

/** \brief			Приостановить исполнение программы на заданный промежуток времени.
  * 
  * \param	time	Время, мс
  * 
  * \usage \code
  *		jeSetFillColor(JE_YELLOW);
  * 	jeDrawRectangle(0, 0, 800, 600);
  *		jeUpdate();
  *		jeSleep(500);
  *		jeSetFillColor(JE_VIOLET);
  * 	jeDrawRectangle(0, 0, 800, 600);
  *		jeUpdate();
  * \endcode
  */
void jeSleep(int time);

POINT jeWindowSize();
int   jeWindowSizeX();
int   jeWindowSizeY();

POINT jeGetTextSize  (const char* text);
int   jeGetTextSizeX (const char* text);
int   jeGetTextSizeY (const char* text);

bool jeIsKeyPressed (int code);
bool jeLButtonPressed();
bool jeRButtonPressed();

POINT jeGetMousePosition();
int jeGetMouseX();
int jeGetMouseY();

void jeShowConsole();
void jeHideConsole();

void jeShowCursor();
void jeHideCursor();

void jeError(const char* description);

LRESULT CALLBACK _jeWndProc(HWND wnd, UINT msg, WPARAM wpar, LPARAM lpar);
void _jeGetModuleFileName(char* name);
void _jeDeleteDCWithoutCheck(HDC dc);
void _jeFreeDataAndExit(bool error);
FARPROC _jeDllImport(const char* dllName, const char* funcName);

#define JE_ERROR_MESSAGE_SIZE 4096

#define JE_ASSERT(statement)											\
	do																	\
	{																	\
		if (!(statement))												\
		{																\
			char __errorMessage[JE_ERROR_MESSAGE_SIZE] = "";			\
			sprintf(__errorMessage, "Assertion failed: %s\nFile: %s\n"	\
					"Line: %d\nFunction: %s\n"							\
					"Что-то пошло не так: либо неправильно "			\
					"использована библиотека, либо сработал твой "		\
					"ассерт. Попытайся понять, в чем проблема "			\
					"по условию ассерта, имени файла, номеру строки"	\
					"и функции, которые указаны выше. Удачи :)", 		\
					#statement, __FILE__, __LINE__, JE_FUNCTION);		\
			jeError(__errorMessage);									\
		}																\
	}																	\
	while (0)

#define JE_FUNCTION_START(name)											\
	if (!_jeState.initialized)											\
		jeError("Перед тем, как использовать функцию " #name 			\
				 ", создайте окно!");

#ifndef JELIB_PROTOTYPES_ONLY
#include "JElib.c"
#endif

#endif // JELIB_H

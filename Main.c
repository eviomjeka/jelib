#include "JElib.h"

int main()
{
	jeCreateWindow(800, 600);

	jeSetFillColor(JE_GREY);
	jeDrawRoundRect(0, 0, 800, 600, 20, 20);

	jeSetColorEx(RGB(255, 0, 0), 3);
	jeDrawLine(10, 10, 50, 50);

	jeSelectRegion(200, 150, 600, 450);
	jeSetFillColor(JE_BLUE);
	jeDrawRectangle(0, 0, 800, 600);
	jeSelectNormalRegion();

	jeSetColor(JE_GREEN);
	jeSelectFont("Arial", 30, true, false, false, false);
	jeDrawText(80, 0, "YES");
	jeSelectFont("Comic Sans MS", 30, false, false, false, true);
	jeDrawText(80, 100, "NO");

	jeInvertRect(200, 150, 400, 250);
	jeSetFillColor(JE_VIOLET);
	jeFloodFill(0, 0);

	jeWait();
	return 0;
}
//! \file    JEImportList.h
//! \brief   JElib import list header file

#ifndef _JE_IMPORT
#define _JE_IMPORT(...)
#endif

_JE_IMPORT     ("GDI32",    HDC,      CreateCompatibleDC,       (HDC dc));
_JE_IMPORT     ("GDI32",    HBITMAP,  CreateCompatibleBitmap,   (HDC dc, int width, int height));
_JE_IMPORT     ("GDI32",    HGDIOBJ,  GetStockObject,           (int object));
_JE_IMPORT     ("GDI32",    HGDIOBJ,  SelectObject,             (HDC dc, HGDIOBJ object));
_JE_IMPORT     ("GDI32",    HGDIOBJ,  GetCurrentObject,         (HDC dc, unsigned objectType));
_JE_IMPORT     ("GDI32",    int,      GetObjectA,               (HGDIOBJ obj, int bufsize, void* buffer));
_JE_IMPORT     ("GDI32",    DWORD,    GetObjectType,            (HGDIOBJ object));
_JE_IMPORT     ("GDI32",    BOOL,     DeleteDC,                 (HDC dc));
_JE_IMPORT     ("GDI32",    BOOL,     DeleteObject,             (HGDIOBJ object));
_JE_IMPORT     ("GDI32",    COLORREF, SetTextColor,             (HDC dc, COLORREF color));
_JE_IMPORT     ("GDI32",    COLORREF, SetBkColor,               (HDC dc, COLORREF color));
_JE_IMPORT     ("GDI32",    int,      SetBkMode,                (HDC dc, int bkMode));
_JE_IMPORT     ("GDI32",    HFONT,    CreateFontA,              (int height, int width, int escapement, int orientation,
		int weight, DWORD italic, DWORD underline, DWORD strikeout,
		DWORD charSet, DWORD outputPrec, DWORD clipPrec,
		DWORD quality, DWORD pitchAndFamily, const char face[]));
_JE_IMPORT     ("GDI32",    COLORREF, SetPixel,                 (HDC dc, int x, int y, COLORREF color));
_JE_IMPORT     ("GDI32",    COLORREF, GetPixel,                 (HDC dc, int x, int y));
_JE_IMPORT     ("GDI32",    HPEN,     CreatePen,                (int penStyle, int width, COLORREF color));
_JE_IMPORT     ("GDI32",    HBRUSH,   CreateSolidBrush,         (COLORREF color));
_JE_IMPORT     ("GDI32",    BOOL,     MoveToEx,                 (HDC dc, int x, int y, POINT* point));
_JE_IMPORT     ("GDI32",    BOOL,     LineTo,                   (HDC dc, int x, int y));
_JE_IMPORT     ("GDI32",    BOOL,     Polygon,                  (HDC dc, const POINT points[], int count));
_JE_IMPORT     ("GDI32",    BOOL,     Rectangle,                (HDC dc, int x0, int y0, int x1, int y1));
_JE_IMPORT     ("GDI32",    BOOL,     RoundRect,                (HDC dc, int x0, int y0, int x1, int y1, int sizeX, int sizeY));
_JE_IMPORT     ("GDI32",    BOOL,     Ellipse,                  (HDC dc, int x0, int y0, int x1, int y1));
_JE_IMPORT     ("GDI32",    BOOL,     Arc,                      (HDC dc, int x0, int y0, int x1, int y1,
int xStart, int yStart, int xEnd, int yEnd));
_JE_IMPORT     ("GDI32",    BOOL,     Pie,                      (HDC dc, int x0, int y0, int x1, int y1,
int xStart, int yStart, int xEnd, int yEnd));
_JE_IMPORT     ("GDI32",    BOOL,     Chord,                    (HDC dc, int x0, int y0, int x1, int y1,
int xStart, int yStart, int xEnd, int yEnd));
_JE_IMPORT     ("GDI32",    BOOL,     TextOutA,                 (HDC dc, int x, int y, const char string[], int length));
_JE_IMPORT     ("GDI32",    UINT,     SetTextAlign,             (HDC dc, unsigned mode));
_JE_IMPORT     ("GDI32",    BOOL,     GetTextExtentPoint32A,    (HDC dc, const char string[], int length, SIZE* size));
_JE_IMPORT     ("GDI32",    BOOL,     ExtFloodFill,             (HDC dc, int x, int y, COLORREF color, unsigned type));
_JE_IMPORT     ("GDI32",    BOOL,     BitBlt,                   (HDC dest, int xDest, int yDest, int width, int height,
		HDC src,  int xSrc,  int ySrc,  DWORD rOp));
_JE_IMPORT     ("GDI32",    HRGN,     CreateRectRgn,            (int x0, int y0, int x1, int y1));
_JE_IMPORT     ("GDI32",    BOOL,     GetBitmapDimensionEx,     (HBITMAP bitmap, SIZE* dimensions));
_JE_IMPORT     ("GDI32",    HBRUSH,   CreatePatternBrush,       (HBITMAP hbm));
_JE_IMPORT     ("GDI32",    int,      SetArcDirection,          (HDC hdc,int dir));

_JE_IMPORT     ("User32",   BOOL,     InvertRect,               (HDC hDC,CONST RECT *lprc));
_JE_IMPORT     ("User32",   int,      DrawTextA,                (HDC dc, const char text[], int length, RECT* rect, unsigned format));
_JE_IMPORT     ("User32",   HANDLE,   LoadImageA,               (HINSTANCE inst, const char name[], unsigned type,
		int sizex, int sizey, unsigned mode));
_JE_IMPORT     ("MSImg32",  BOOL,     TransparentBlt,           (HDC dest, int destX, int destY, int destWidth, int destHeight,
		HDC src, int srcX,  int srcY,  int srcWidth,  int srcHeight,
		unsigned transparentColor));
_JE_IMPORT     ("MSImg32",  BOOL,     AlphaBlend,               (HDC dest, int destX, int destY, int destWidth, int destHeight,
		HDC src,  int srcX,  int srcY,  int srcWidth,  int srcHeight,
		BLENDFUNCTION blending));

_JE_IMPORT     ("Kernel32", HWND,     GetConsoleWindow,         (void));
